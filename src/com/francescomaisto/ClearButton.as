package com.francescomaisto 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class ClearButton extends Sprite 
	{
		private var _formMediator:FormMediator;
		
		public function ClearButton(formMediator:FormMediator) 
		{
			_formMediator = formMediator;
			
			disable();
			
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function onClick(e:MouseEvent):void 
		{
			_formMediator.clearButtonClicked();
		}
		
		public function enable():void
		{
			graphics.clear();
			graphics.beginFill(0xcc0000);
			graphics.drawRect(0, 0, 100, 25);
			graphics.endFill();
		}
		
		public function disable():void
		{
			graphics.clear();
			graphics.beginFill(0xaaaaaa);
			graphics.drawRect(0, 0, 100, 25);
			graphics.endFill();			
		}		
	}
}