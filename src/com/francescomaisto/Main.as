package com.francescomaisto
{
	import flash.display.Sprite;
	 
	 /* Based on video tutorial: 
	  * http://youtu.be/lsrbDkW7pVw
	  */
	
	public class Main extends Sprite 
	{
		protected var submitButton:SubmitButton;		
		protected var clearButton:ClearButton;		
		protected var inputText:InputTextField;		
		protected var formMediator:FormMediator;
				
		public function Main():void 
		{
			formMediator = new FormMediator();
			
			submitButton = new SubmitButton(formMediator);
			submitButton.x = 135;
			submitButton.y = 125;			
			addChild(submitButton);
			
			clearButton = new ClearButton(formMediator);
			clearButton.x = 10;
			clearButton.y = 125;
			addChild(clearButton);			
			
			inputText = new InputTextField(formMediator);
			inputText.x = 10;
			inputText.width = 225;
			addChild(inputText);
			
			formMediator.submitButton = submitButton;
			formMediator.clearButton = clearButton;
			formMediator.inputText = inputText;			
		}		
	}	
}