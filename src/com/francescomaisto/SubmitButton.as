package com.francescomaisto 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class SubmitButton extends Sprite 
	{
		private var _formMediator:FormMediator;
		
		public function SubmitButton(formMediator:FormMediator) 
		{
			_formMediator = formMediator;
			
			disable();
			
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function onClick(e:MouseEvent):void 
		{
			_formMediator.submitButtonClicked();
		}
		
		public function enable():void
		{
			graphics.clear();
			graphics.beginFill(0x00cc00);
			graphics.drawRect(0, 0, 100, 25);
			graphics.endFill();
		}
		
		public function disable():void
		{
			graphics.clear();
			graphics.beginFill(0xaaaaaa);
			graphics.drawRect(0, 0, 100, 25);
			graphics.endFill();			
		}		
	}
}