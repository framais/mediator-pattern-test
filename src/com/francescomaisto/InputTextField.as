package com.francescomaisto 
{
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	public class InputTextField extends TextField 
	{
		private var _formMediator:FormMediator;
		
		public function InputTextField(formMediator:FormMediator) 
		{
			_formMediator = formMediator;
			
			type = TextFieldType.INPUT;
			border = true;
			
			addEventListener(Event.CHANGE, onChange);
		}
		
		private function onChange(e:Event):void 
		{
			_formMediator.textInputChanged();
		}
		
		override public function set text(value:String):void
		{
			super.text = value;
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
}