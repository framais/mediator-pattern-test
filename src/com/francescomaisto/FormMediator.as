package com.francescomaisto 
{
	public class FormMediator 
	{
		public var submitButton:SubmitButton;
		public var clearButton:ClearButton;
		public var inputText:InputTextField;
		
		public function textInputChanged():void 
		{
			if (inputText.text.length == 0)
			{
				clearButton.disable();
				submitButton.disable();
			}
			else
			{
				clearButton.enable();
				submitButton.enable();				
			}			
		}
		
		public function clearButtonClicked():void 
		{
			inputText.text = "";
		}
		
		public function submitButtonClicked():void 
		{
			inputText.text = "Submitted!";
			submitButton.disable();
		}		
	}
}